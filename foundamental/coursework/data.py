import random

import requests

from foundamental.coursework.utils import BasicWord


def load_random_word():
    response = requests.get("https://www.jsonkeeper.com/b/TRTZ")
    data = response.json()
    random.shuffle(data)
    res = BasicWord(data[0]["word"], data[0]['subwords'])
    return res

