from utils import Player, BasicWord
from data import load_random_word


def main():
    basic = load_random_word()
    print('Привет начинаем играть \nВведите ваше имя')
    name = input()
    player = Player(name)
    print(f'составьте {basic.count_word()} слов из слова {basic.baseWord}\n слова должны быть не короче 3х букв'
          f'\nчто бы закончить игру угадайте все слова или напишите "stop"\nПоехали ваше первое слово? ')
    print(basic.collectWord)
    while True:
        userWord = input('ваше слово? ')
        if userWord != 'stop' and basic.collectWord != player.countUseWord():
            if len(userWord) >= 3:
                if basic.checkWord(userWord) == True:
                    print(f'верно, слово {userWord}, верное')
                    player.addWord(userWord)
                else:
                    print(f'Извините слово не верное')
            else:
                print('слово не может быть короче трех букв')
        else:
            break
    print(f'Игра завершена вы угадали {player.countUseWord()} слов')


main()
