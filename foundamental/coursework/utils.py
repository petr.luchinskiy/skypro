from pprint import pprint
import random
import requests


class BasicWord:
    def __init__(self, baseWord, collectWord):
        self.baseWord = baseWord
        self.collectWord = collectWord

    def __repr__(self):
        return f"Класс работы с словами"

    def checkWord(self, userWord):
        if userWord in self.collectWord:
            return True
        else:
            return False

    def count_word(self):
        return len(self.collectWord)


class Player:
    def __init__(self, name, userWord=None):
        self.name = name
        self.userWord = userWord
        self.guessWord = []

    def __repr__(self):
        return f"класс работы с игроком"

    def countUseWord(self) -> int:
        return len(self.guessWord)

    def addWord(self, userWord):
        self.guessWord.append(userWord)
        print(self.guessWord)