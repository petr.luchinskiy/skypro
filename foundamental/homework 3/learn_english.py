# приветствие
CURRENT_BALL = 0
CURRENT_ANSWER = 0
TRY_COUNT = 3

ask = ['Вопрос1 : Му name - Vova', 'Вопрос2 : I __ a coder', 'Вопрос3 : I live __ Moscow']
answer = ['is', 'am', 'in']

print('Привет! Предлагаю проверить свои знания английского! Наберите "ready" что бы начать')
start = input()
if start == 'ready':
    name = input('Расскажи, как тебя зовут!')

    print(f'Привет, {name}, начинаем тренировку!')

    # блок вопроса
    for i in range(0, len(ask)):
        print(ask[i])
        answer_user = input()
        if answer_user == answer[i]:
            CURRENT_BALL += TRY_COUNT
            CURRENT_ANSWER += 1
        else:
            TRY_COUNT = TRY_COUNT - 1
            if TRY_COUNT > 0:
                print(f'Неправильно,у вас осталось {TRY_COUNT} попыток, попробуйте еще раз,')
                print(ask[i])
                answer_user = input()
                if answer_user == answer[i]:
                    CURRENT_BALL += TRY_COUNT
                    CURRENT_ANSWER += 1
                else:
                    TRY_COUNT = TRY_COUNT - 1
            else:
                print(f'Увы но нет, верный ответ {answer[i]}')
                break

    percent = (CURRENT_ANSWER / len(answer)) * 100
    print(f'Вот и все! вы ответили верно на {CURRENT_ANSWER} из 3х вопросов')
    print(f'Вы заработали баллов: {CURRENT_BALL} \n это {percent} процентов', )
else:
    print('Кажется вы не хотите играть, очень жаль')