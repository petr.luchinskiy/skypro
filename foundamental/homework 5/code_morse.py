from random import shuffle


def morse_encode(sentence):
    morse = {
        "0": "-----",
        "1": ".----",
        "2": "..---",
        "3": "...--",
        "4": "....-",
        "5": ".....",
        "6": "-....",
        "7": "--...",
        "8": "---..",
        "9": "----.",
        "a": ".-",
        "b": "-...",
        "c": "-.-.",
        "d": "-..",
        "e": ".",
        "f": "..-.",
        "g": "--.",
        "h": "....",
        "i": "..",
        "j": ".---",
        "k": "-.-",
        "l": ".-..",
        "m": "--",
        "n": "-.",
        "o": "---",
        "p": ".--.",
        "q": "--.-",
        "r": ".-.",
        "s": "...",
        "t": "-",
        "u": "..-",
        "v": "...-",
        "w": ".--",
        "x": "-..-",
        "y": "-.--",
        "z": "--..",
        ".": ".-.-.-",
        ",": "--..--",
        "?": "..--..",
        "!": "-.-.--",
        "-": "-....-",
        "/": "-..-.",
        "@": ".--.-.",
        "(": "-.--.",
        ")": "-.--.-",
        " ": "/"
    }
    s = []
    for i in sentence:
        s.append(morse[i])
    return ''.join(s)


def get_word():
    word = ['snake', 'ball', 'hi ho', 'tank', 'dirty', 'call', 'world']
    shuffle(word)
    return word[0]


answer = {}
for i in range(0, 10):
    s = morse_encode(get_word())
    print('Добро пожаловать давай сыграем')
    print(f'Что это за слово? {s}')
    ask = input('')
    res = morse_encode(ask)
    if res == s:
        answer[i] = True
        print('верно')
    else:
        answer[i] = False
        print('не верно')
right = sum(value == True for value in answer.values())
print(f'Всего ответов {len(answer)}, \n из них верных: {right} \n не верных {len(answer) - right}')
