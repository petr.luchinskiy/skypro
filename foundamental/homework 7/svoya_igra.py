import json
from pprint import pp

points, correct, incorrect = 0, 0, 0
questions_total = 9
questions_asked = 0
def load_questions_from_json():
    file = open('question.json', 'r', encoding='utf-8')
    data = json.load(file)
    file.close()
    return data

def draw_table(questions):
    for category_name, category_questions in questions.items():
        print(category_name.ljust(17), end=' ')
        for price, question_data in category_questions.items():
            asked = question_data["asked"]
            if not asked:
                print(price.ljust(5), end=' ')
            else:
                print("     ".ljust(5), end="  ")
        print()

def parse_input(user_input):
    user_data = user_input.split(" ")
    if len(user_data) != 2:
        return False
    else:
        return {"category": user_data[0], "price": user_data[1]}


def print_question(question_text):
    print(f'Слово {question_text} означает ...')


def show_stats(points, correct, incorrect):
    print("У нас закончились вопросы")
    print("")
    print(f"Ваши баллы {points}")
    print(f"Правильные ответы {correct}")
    print(f"Неправльные ответы {incorrect}")


def save_results_to_file(points, correct, incorrect):
    file = open('results.json', 'r')
    results = json.load(file)
    file.close()

    results.append({"points": points, "correct": correct, "incorrect": incorrect})

    file = open('results.json', 'w')
    json.dump(results, file)
    file.close()


questions = load_questions_from_json()
some_questions_left = True
while questions_asked<questions_total:

    draw_table(questions)
    user_input = input().title()
    user_data = parse_input(user_input)

    if not user_data:
        print('неверный код')
        continue

    category, price = user_data["category"], user_data["price"]
    question = questions[category][price]

    if question["asked"]:
        print('Ты уже это спрашивал')
        continue

    print_question(question["question"])
    user_answer = input().lower()
    if user_answer == question["answer"]:
        print('верно')
        points = points+int(price)
        correct = correct + 1
    else:
        print('неверно')
        points = points-int(price)
        incorrect = incorrect - 1
    question["asked"] = True
    questions_asked +=1


show_stats(points, correct, incorrect)
save_results_to_file(points, correct, incorrect)