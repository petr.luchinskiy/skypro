import json
import random
from pprint import pprint

from utils import Question

def main():
    questions = []
    file = open('r.json', 'r', encoding='utf-8')
    questions_data = json.load(file)
    file.close()

    for question_data in questions_data:
        questions.append(Question(
            question_data["q"],
            int(question_data["l"]),
            question_data["a"]
        ))

    random.shuffle(questions)
    for question in questions:
        print(question.build_questions())
        user_answer = input('Ваш ответ ')
        question.user_answer = user_answer

        if question.is_correct():
            print(question.build_positive_feedback())
        else:
            print(question.build_negative_feedback())

    counter = 0
    points = 0

    for question in questions:
        if question.is_correct():
            counter +=1
            points += question.get_points()

    print(f'Вот и все \nоВерно твечено {counter} из {len(questions)} \n набрано баллов {points}')


main()