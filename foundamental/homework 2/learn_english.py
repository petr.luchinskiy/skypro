# приветствие
CURRENT_BALL = 0
CURRENT_ANSWER = 0
print('Привет! Предлагаю проверить свои знания английско го!')
name = input('Расскажи, как тебя зовут!')

print(f'Привет, {name}, начинаем тренировку!')

# блок вопроса
answer1 = input('Вопрос1 : Му name - Vova')
if answer1 == 'is':
    print('Ответ верный! /n Вы получаете 10 баллов')
    CURRENT_BALL += 10
    CURRENT_ANSWER += 1
else:
    print('Неправильно, правильный ответ "is"')

answer2 = input('Вопрос2 : I __ a coder')
if answer2 == 'am':
    print('Ответ верный! /n Вы получаете 10 баллов')
    CURRENT_BALL += 10
    CURRENT_ANSWER += 1
else:
    print('Неправильно, правильный ответ "a"')

answer3 = input('Вопрос3 : I live __ Moscow')
if answer3 == 'in':
    print('Ответ верный! /n Вы получаете 10 баллов')
    CURRENT_BALL += 10
    CURRENT_ANSWER += 1
else:
    print('Неправильно, правильный ответ "in"')

percent = (CURRENT_ANSWER/3)*100
print(f'Вот и все! вы ответили верно на {CURRENT_ANSWER} из 3х вопросов')
print(f'Вы заработали баллов: {CURRENT_BALL} \n это {percent} процентов',)