easy = {
    'hard':'жесткий',
    'home': 'дом',
    'love': 'любовь',
    'task': 'задание'
}
medium = {
    'job':'работа',
    'town':'городок',
    'small': 'маленький',
    'river':'река'
}
hard = {
    'destroy':'разрушить',
    'ugly':'ужасный',
    'mouse':'мышь',
    'dictionary':'словарь'
}
word = {}
levels = {
    1:'Нулевой',
    2:'Так себе',
    3:'Можно лучше',
    4:'Хорошо'
}
answers = {}
results = {}

mode = input('Выберите уровень сложности: введите легкий, средний, сложный')
if mode == 'легкий':
    word = easy
elif mode == 'средний':
    word = medium
else:
    word = hard
print('Выбран уровень сложности, мы предложим 4 слова. подберите перевод')
agree = input('Введите энтер')

for i in word:
    print(f'{i}, {len(word[i])}, начинается на {word[i][0:1]}...')
    ans = input()
    print(word[i])
    if word[i] == ans:
        print(f'Верно, {i} это {ans}')
        results[i] = True
    else:
        print(f'Неверно {i} это {word[i]}')
        results[i] = False
    answers[i] = ans
print('Правильно отвеченные слова')
for i in results:
    if results[i]==True:
        print(i)
    else:
        pass
print('Неправильно отвечены слова')
for i in results:
    if results[i]==False:
        print(i)
    else:
        pass
right = sum(value == True for value in results.values())
print(f'Ваш ранг {levels[right]}')
print('Конец')