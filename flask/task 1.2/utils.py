import json

def load_data_candidates():
    with open('data.json','r', encoding='utf-8') as f:
        data = json.load(f)
    return data

def get_candidate(candidate_id):
    res = []
    data = load_data_candidates()
    for candidate in data:
        if int(candidate["id"])==int(candidate_id):
            res = candidate
        else:
            continue
    if res is None:
        return 'net'
    if res:
        return res

def get_candidate_name(name):
    data = load_data_candidates()
    str_candidates = ''
    for candidate in data:
        candidate_skills = candidate['name'].split(' ')
        if name in candidate_skills:
            str_candidates += f"{candidate['name']} <br> {candidate['position']} <br>{candidate['skills']} <br> <br>"
    return str_candidates

def get_candidate_skill(skill):
    data = load_data_candidates()
    str_candidates = ''
    for candidate in data:
        candidate_skills = candidate['skills'].split(', ')
        candidate_skills = [x.lower() for x in candidate_skills]
        if skill in candidate_skills:
            str_candidates += f"{candidate['name']} <br> {candidate['position']} <br>{candidate['skills']} <br> <br>"
    return str_candidates
