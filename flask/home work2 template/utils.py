import json

__data = []


def load_data_candidates():
    global __data
    with open('data.json', 'r', encoding='utf-8') as f:
        __data = json.load(f)
    return __data


def get_candidate(candidate_id):
    for candidate in __data:
        print(candidate['id'])
        if candidate['id'] == candidate_id:
            return {
                'name': candidate['name'],
                'position': candidate['position'],
                'picture': candidate['picture'],
                'skills': candidate['skills'],
            }
    return {'not_found': 'ушел на обед'}

def get_candidate_by_search(candidate_name):
    return [candidate for candidate in __data if candidate_name.lower() in candidate['name'].lower()]
def get_candidate_by_skills(skill_name):
    candidaes= []
    for candidate in __data:
        skill = candidate['skills'].lower().split(', ')
        if skill_name.lower() in skill:
            candidaes.append(candidate)
        return candidaes

