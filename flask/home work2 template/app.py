from flask import Flask, render_template
from utils import load_data_candidates, get_candidate, get_candidate_by_search,  get_candidate_by_skills

app = Flask(__name__)

data = load_data_candidates()

@app.route("/")
def index():
    return render_template('index.html', candidates=data)

@app.route("/candidate/<int:uid>")
def profile(uid):
    candidate = get_candidate(uid)
    print(candidate)
    return render_template('profile.html', candidate=candidate)

@app.route("/search/<name>")
def search(name):
    candidate = get_candidate_by_search(name)
    return render_template('search.html', candidate=candidate, candidate_len = len(candidate))



@app.route("/skills/<skill>")
def skill(skill):
    candidate = get_candidate_by_skills(skill)
    return render_template('search.html', candidate=candidate, candidate_len = len(candidate))

app.run(debug=True)