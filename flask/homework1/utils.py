import json
from pprint import pprint

def data_json():
    with open("candidates.json", 'r', encoding='utf-8') as f:
        data = json.load(f)
        return data

def data_json_dict():
    with open("candidates.json", 'r', encoding='utf-8') as f:
        data = json.load(f)
        candidates = {}
        for i in data:
            candidates[i['id']] = i
        return candidates

