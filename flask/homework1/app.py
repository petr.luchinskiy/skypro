from flask import Flask
from utils import data_json, data_json_dict

app = Flask(__name__)
candidates = data_json()
candidates_dict = data_json_dict()
@app.route("/")
def page_index():

    str_candidates = '<pre>'
    for candidate in candidates_dict.values():
        str_candidates += f"{candidate['name']} \n {candidate['position']} \n{candidate['skills']} \n \n"
    str_candidates += "</pre>"
    return str_candidates

@app.route("/candidates/<int:id>")
def profile(id):
    candidate = candidates[id]
    str_candidates = f"<img src={candidate['pictures']}/</img> <br> <br>{candidate['name']}<br>{candidate['position']} " \
                     f"{candidate['skills']}<br><br>"
    return str_candidates

@app.route("/skills/<skill>")
def skill(skill):
    str_candidates = ''
    for candidate in candidates_dict.values():
        candidate_skills = candidate['skills'].split(', ')
        candidate_skills = [x.lower() for x in candidate_skills]
        if skill in candidate_skills:
            str_candidates = f"<img src={candidate['pictures']} </img> </br></br> {candidate['name']} <br> {candidate['position']} <br>{candidate['skills']} <br> <br>"
    str_candidates += ""
    return str_candidates

app.run()

